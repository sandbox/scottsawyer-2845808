Bit.ly for Drupal by Robin Monks <http://robinmonks.com/>

Drupal 8 port by Scott Sawyer <http://scottsawyerconsulting.com/>

Bit.ly for Drupal, once installed, provides easy access to the bit.ly
API to other Drupal modules.

IMPORTANT NOTE: This module requires you have a bit.ly account to work
properly, after installation, please go to /admin/settings/bitly to
authenticate and begin using the module.

The Drupal 8 port relies on the Bit.ly package https://packagist.org/packages/gabrielkaputa/bitly, which is expected to be installed with composer.

