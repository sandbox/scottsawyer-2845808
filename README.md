# Bit.ly For Drupal

This is a Drupal 8 version of Bit.ly.  The module was inspired by [Bit.ly For Drupal](https://www.drupal.org/project/bitly).

It uses [Gabriel Kaputa's  Bit.ly PHP library](https://packagist.org/packages/gabrielkaputa/bitly).  To install the package, run: `composer update` after installing this module.

You have your choice of using Bit.ly Generic Access Token or the OAuth keys.  Either way, you must create a Bit.ly account.  You can access registered OAuth App and Generic Access Token (https://bitly.com/a/oauth_apps).

Once you have your credentials, visit the module configuration screen at /admin/config/services/bitly

This module is an API only module.  To use it, you need to inject the service yourself and pass it a URL to shorten.

For example:

```php
     $shorten = \Drupal::service('bitly.bitly_shorten');
     $shorUrl = $shorten->bitlyShorten($longUrl);
```

TODO:
 Add support for Key module (https://drupal.org/project/key)

