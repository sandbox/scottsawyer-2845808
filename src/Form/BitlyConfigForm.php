<?php
/**
 * @file
 * Contains \Drupal\Bitly\Form\BitlyConfigForm
 */

namespace Drupal\bitly\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;


/**
 * Configuration form for Bitly.
 */
class BitlyConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bitly_config_form';
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('bitly.settings');

    $moduleHandler = \Drupal::service('module_handler');

    $form['bitly_auth_type'] = array(
      '#type' => 'radios',
      '#title' => $this->t('BIT.LY AUTHENTICATION TYPE'),
      '#description' => $this->t('Choose an authentication type.'),
      '#default_value' => $config->get('bitly.auth_type'),
      '#options' => array(
        0 => $this->t('Generic Access Token'),
        1 => $this->t('OAuth')
      ),
    );

    if ($moduleHandler->moduleExists('key')) {
    }
    else {
      $form['bitly_key'] = array(
        '#type' => 'hidden',
        '#value' => FALSE,
      );
      $form['bitly_auth_generic_access_token'] = array(
        '#type' => 'container',
        '#title' => $this->t('BIT.LY GENERIC ACCESS TOKEN'),
        '#states' => array(
          'visible' => array(
            'input[name="bitly_auth_type"]' => array('value' => '0'),
          ),
        ),
      );
      $form['bitly_auth_generic_access_token']['bitly_access_token'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Generic Access Token'),
        '#default_value' => $config->get('bitly.access_token'),
      );
      $form['bitly_auth_oauth'] = array(
        '#type' => 'container',
        '#title' => $this->t('BIT.LY OAUTH'),
        '#states' => array(
          'visible' => array(
            'input[name="bitly_auth_type"]' => array('value' => '1'),
          ),
        ),
      );
      $form['bitly_auth_oauth']['bitly_api_credentials'] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('BIT.LY API CREDENTIALS'),
        '#description' => $this->t('Please supply the bit.ly API credentials that will be used globally. These API credentials will be used in any case where a user either does not or cannot supply their own credentials.'),
      );
      $form['bitly_auth_oauth']['bitly_api_credentials']['bitly_login'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Bit.ly Login'),
        '#description' => $this->t('This field is case sensitive.'),
        '#default_value' => $config->get('bitly.login'),
      );
      $form['bitly_auth_oauth']['bitly_api_credentials']['bitly_password'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Bit.ly Password'),
        '#description' => $this->t('This field is case sensitive.'),
        '#default_value' => $config->get('bitly.password'),
      );
      $form['bitly_auth_oauth']['bitly_oauth_credentials'] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('BIT.LY OAUTH CREDENTIALS'),
        '#description' => $this->t('Please supply OAuth to allow users to log into their bit.ly accounts by clicking a sign-in link rather than needing to enter an API key. Currently you must request OAuth access directly from bit.ly.'),
      );
      $form['bitly_auth_oauth']['bitly_oauth_credentials']['bitly_oauth_client_id'] = array(
         '#type' => 'textfield',
         '#title' => $this->t('Bit.ly OAuth client_id'),
         '#description' => $this->t('This field is case sensitive.'),
         '#default_value' => $config->get('bitly.client_id'),
      );
      $form['bitly_auth_oauth']['bitly_oauth_credentials']['bitly_oauth_client_secret'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Bit.ly OAuth client_secret'),
        '#description' => $this->t('This field is case sensitive.'),
        '#default_value' => $config->get('bitly.client_secret'),
      );
    }

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('bitly.settings');
    if ($form_state->getValue('bitly_key') == FALSE) {
      $config->set('bitly.auth_type', $form_state->getValue('bitly_auth_type'));
      if ($form_state->getValue('bitly_auth_type') == 0) {
        $config->set('bitly.access_token', $form_state->getValue('bitly_access_token'));
      }
      else {
        $config->set('bitly.key_module', $form_state->getValue('bitly_key'));
        $config->set('bitly.login', $form_state->getValue('bitly_login'));
        $config->set('bitly.password', $form_state->getValue('bitly_password'));
        $config->set('bitly.client_id', $form_state->getValue('bitly_oauth_client_id'));
        $config->set('bitly.client_secret', $form_state->getValue('bitly_oauth_client_secret'));
      }
    }
    $config->save();

    return parent::submitForm($form, $form_state);
    
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'bitly.settings',
    ];
  }

}

?>
