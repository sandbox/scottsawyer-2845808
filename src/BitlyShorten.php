<?php

namespace Drupal\bitly;

use GabrielKaputa\Bitly\Bitly;

/**
 * Class BitlyShorten.
 *
 * @package Drupal\bitly
 */
class BitlyShorten {

  /**
   * Constructor.
   */
  public function __construct() {

  }
  
  /**
   * Shorten URL.
   */
  public function bitlyShorten($longUrl) {

    $config = \Drupal::config('bitly.settings');
    $auth_type = $config->get('bitly.auth_type');
    if ($auth_type == 0) {
      $access_token = $config->get('bitly.access_token');
      $bitly = Bitly::withGenericAccessToken($access_token);
    }
    else {
      $client_id = $config->get('bitly.client_id');
      $client_secret = $config->get('bitly.client_secret');
      $login = $config->get('bitly.login');
      $password = $config->get('bitly.password');
      $bitly = Bitly::withCredentials($client_id, $client_secret, $login, $password);
    }
    return $bitly->shortenUrl($longUrl);
    
  }
}
